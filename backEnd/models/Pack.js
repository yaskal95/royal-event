const mongoose = require('mongoose');

const packSchema = mongoose.Schema({
    name: {
        type : String,
        required : true
    },
    
    description: {
        type : String,
        required : true
    }

})

module.exports = mongoose.model('Pack',packSchema);