const mongoose = require('mongoose');

const reservationSchema = mongoose.Schema({

    customerName: {
        type : String,
        required : true
    },

    cin: {
        type : String,
        required : true
    },

    phoneNumber: {
        type : String,
        required : true
    },

    description: {
        type : String,
        required : true
    },

    priceAdvance : {
        type: Number,
        required : true
    },

    totalPrice : {
        type: Number,
        required : true
    },

    status : {
        type: String,
        required : true
    },

    createdAt : {
        type: Date,
        required : true
    },

    reservationAt :{
        type:Date,
        required : true
    },
    city: {
        type: mongoose.Schema.Types.ObjectId,
        ref:'City',
        required : false
    },
    address: {
        type:String,
        required : true
    },
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref:'User',
        required : false
    }
})

module.exports = mongoose.model('Reservation',reservationSchema)