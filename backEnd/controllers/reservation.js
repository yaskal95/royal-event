const Reservation = require('../models/Reservation');
const fs = require('fs');
const XLSX = require('xlsx');
const { Blob } = require('buffer');
const fileSaver = require('file-saver');
const path = require('path');
const converter = require('json-2-csv');
const csvToJson = require('csvtojson');

// Get All Reservations
exports.getAll = async (req, res, next) => {

    let reservations = [];
    let populate = "";
    let skip = 0;
    let limit = 3;
    let pages = 1;

    if (req.query.expands) populate = req.query.expands.split(",");
    populate = populate.map(pop => {
        return { path: pop, select: 'name' }
    });

    if ('undefined' !== req.query.status && 'all' !== req.query.status) {

        let count = await Reservation.find({ status: req.query.status }).count();
        pages = Math.ceil(count / limit);
        if (req.query.page !== undefined) skip = (limit * req.query.page) - limit;

        reservations = await Reservation.find({ status: req.query.status })
            .sort({ createdAt: -1 })
            .populate(populate)
            .skip(skip)
            .limit(limit)
            ;
    } else {
        let count = await Reservation.count();
        pages = Math.ceil(count / limit);
        if (req.query.page !== undefined) skip = (limit * req.query.page) - limit;
        reservations = await Reservation.find()
            .sort({ createdAt: -1 })
            .populate(populate)
            .skip(skip)
            .limit(limit)
            ;
    }

    if (reservations.length === 0) {
        return res.status(404).json({
            message: 'No Reservations'
        })
    } else return res.status(200).json({
        reservations: reservations,
        totalPages: pages
    })
}

// Create new Reservation
exports.add = async (req, res, next) => {

    const reservation = new Reservation({
        customerName: req.body.customerName,
        cin: req.body.cin,
        phoneNumber: req.body.phoneNumber,
        description: req.body.description,
        priceAdvance: req.body.priceAdvance,
        totalPrice: req.body.totalPrice,
        createdAt: new Date(),
        reservationAt: req.body.reservationAt,
        city: req.body.city,
        address: req.body.address,
        status: 'new',
    });

    const result = await reservation.save();

    if (result) return res.status(200).json({
        message: 'Success'
    });
    else return res.status(404).json({
        message: 'Error'
    });

}

// Get Reservation by Id
exports.getReservation = async (req, res, next) => {

    const reservation = await Reservation.findOne({ _id: req.params.id })

    if (reservation) return res.status(200).send(reservation);
    else return res.status(404).json({
        message: 'Not Found'
    });
}

// Edit Reservation by Id
exports.edit = async (req, res, next) => {
    let reservation = new Reservation();
    reservation = req.body;

    const result = await Reservation.findOneAndUpdate({ _id: req.params.id }, {
        $set: reservation
    });

    if (result) return res.status(200).json({
        message: 'Reservation Updated'
    });
    else return res.status(404).json({
        message: 'Error Db Update'
    });
}

// Delete reservation by id
exports.delete = async (req, res, next) => {
    const result = await Reservation.findOneAndDelete({ _id: req.params.id });

    if (result) return res.status(200).json({
        message: 'Reservation Delted'
    });
    else return res.status(404).json({
        message: 'Error Db Delete'
    });
};

// Get Statitics

exports.getStat = async (req, res, next) => {
    
    try{

        const result = await Reservation.aggregate(
            [
                {
                    $group:
                    {
                        _id: null,
                        totalAmount: { $sum:"$totalPrice" },
                        count: { $sum: 1 }
                    }
                }
            ]
        );
        return res.status(200).send(result);

    }catch(err){
        return res.status(400).send(err);
    }
}

exports.exportReservations = async (req, res, next) => {

    const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    const EXCEL_EXTENSION = '.xlsx';
    const filePath = "./public/excel/excel-from-js.xlsx"
    const workSheetClomuns = ["ID", "CustomerName", "PhoneNumber"];
    // return {buffer:jsonToExcel(reservations,'reservationsList'),model:reservations};
    const reservations = await Reservation.find();
    const data = reservations.map(reservation => {
        return [reservation._id, reservation.customerName, reservation.phoneNumber];
    });
    const workSheetData = [
        workSheetClomuns,
        ...data
    ];
    const wb = XLSX.utils.book_new();

    const ws = XLSX.utils.aoa_to_sheet(workSheetData);
    XLSX.utils.book_append_sheet(wb, ws, "Reservations");

    XLSX.writeFile(wb, path.resolve(filePath));

    // const dataBlob = new Blob([buffer],EXCEL_TYPE); 
    // fileSaver.saveAs(dataBlob,"ReservationsExcel"+EXCEL_EXTENSION);
    return res.status(200);
};

exports.exportToCsv = async (req, res, err) => {

    const data = await Reservation.find().select("customerName phoneNumber reservationAt totalPrice");
    const dateExport = new Date().toLocaleDateString().split('/').join('-');
    const fileName = `Reservations_${dateExport}.csv`;

    let newData = data.map(reservation => {
        return {
            customerName: reservation.customerName,
            phoneNumber: reservation.phoneNumber,
            reservationAt: reservation.reservationAt,
            totalPrice: reservation.totalPrice
        }
    });
    

    let json2csvCallback = function (err, csv) {
        if (err) throw err;
        console.log(csv);
    };
    
    const csv = await converter.json2csvAsync(newData);
    fs.writeFileSync(fileName, csv);
    res.header('Content-Type', 'text/csv');
    res.attachment(fileName);
    res.send(csv);
    fs.unlink(fileName, () => undefined);

}

exports.csvToJson = async (req, res, next) => {
    
    const jsonCsv = await csvToJson().fromFile("./test.csv");
    
}

