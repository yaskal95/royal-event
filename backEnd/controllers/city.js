const City = require('../models/City');

exports.add = async (req, res, next) => {

    // Create City instance
    const city = new City({
        name: req.body.name,
    });

    // Save User
    const result = await city.save();
    if (result !== undefined) {
        return res.status(200).json({
            message: "City created"
        });
    } else {
        return res.status(400).json({
            message: result.message
        });
    }
}

// Get All Cities
exports.all = async (req, res, next) => {

    const cities = await City.find();

    if (cities.length === 0) {
        return res.status(404).json({
            message: 'No Cities'
        })
    } else return res.status(200).json({
        cities: cities
    })
}