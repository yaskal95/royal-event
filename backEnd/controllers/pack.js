const Pack = require('../models/Pack');

// Sign Up Methode 
exports.add = async (req, res, next) => {
    
    // Create User instance
    const pack = new Pack({
        name: req.body.name,
        description: req.body.description,
    });

    // Save User
    const result = await pack.save();
    if (result !== undefined) {
        res.status(200).json({
            message: "Pack created"
        });
    }else {
        res.status(400).json({
            message: result.message
        });
    }
}
