const User = require('../models/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const path = require('path');

// Sign Up Methode 
exports.signUp = async (req, res, next) => {

    const user = await User.findOne({ username: req.body.username });
    if (!user) {
        const hash = await bcrypt.hash(req.body.password, 10);
        if (hash === undefined) {
            return res.status(404).json({
                message: "undefined hash"
            });
        } else {
            // Create User instance
            const user = new User({
                username: req.body.username,
                email: req.body.email,
                phoneNumber: req.body.phoneNumber,
                role: req.body.role,
                imageUrl: '',
                password: hash,
            });

            // Save User
            const ress = await user.save();
            if (ress !== undefined) {
                return res.status(200).json({
                    message: "User created"
                });
            }
        }
    } else {
        return res.status(404).json({
            message: "User already exist"
        });
    }

}
// Sign In Methode
exports.signIn = async (req, res, next) => {

    const user = await User.findOne({ username: req.body.username }).select('+password');

    if (user) {
        const result = await bcrypt.compare(req.body.password, user.password);
        if (result) {
            user.password = null;
            const jwtBearerToken = jwt.sign(
                { userId: user._id },
                'eventSecret',
                { expiresIn: '24h' }
            );
            
            return res.status(200).json({
                user: user,
                token: jwtBearerToken,
                expiresIn: '24h'
            });
        } else {
            return res.status(404).json({
                message: "Wrong Password"
            });
        };

    } else {
        return res.status(404).json({
            message: "Wrong Username"
        });
    }

};

// Update User Methode

exports.update = async (req, res, next) => {
    const hash = bcrypt.hash(req.body.password, 10);
    if (hash !== undefined) {
        // Create User instance
        const newUser = new User({
            username: req.body.username,
            password: hash,
        });
    }
    // Update User
    const user = await User.findOneAndUpdate({ _id: req.params.id }, { $set: newUser });
    if (user !== undefined) {
        res.status(202).json({
            message: "User updated"
        })
    } else {
        res.status(404).json({
            message: "Error Update"
        })
    }
};

// Delete User Methode

exports.delete = async (req, res, next) => {
    const result = await User.findOneAndDelete({ _id: req.params.id });
    if (!result) res.status(404).json({
        message: "ERROR TO DELETE"
    });
    else res.status(200).json({
        message: "User Deleted"
    })
};

// Return all Managers
exports.allManagers = async (req, res, next) => {
    let populate = "";
    let skip = 0;
    let limit = 3;
    let count = await User.find({ role: "manager" }).count();
    let pages = Math.ceil(count / limit);

    if (req.query.page !== undefined) skip = (limit * req.query.page) - limit;

    const users = await User.find({ role: "manager" })
        .skip(skip)
        .limit(limit)
        ;

    if (users.length > 0) {
        return res.status(200).json({
            users: users,
        });

    } else {
        return res.status(401).json({
            message: "Managers not found"
        });
    }

};

// get Manager by Id 
exports.getManagerById = async (req, res, next) => {

    if (!req.params.id) return res.status(404).json({ message: 'Manager Not Found' })

    const manager = await User.findOne({ _id: req.params.id, role: "manager" });

    if (!manager) return res.status(404).json({ message: 'Manager Not Found' });

    return res.status(200).send(manager);
}


// Update Manager Methode

exports.updateManager = async (req, res, next) => {

    // Find user and check if has role Managers
    let manager = await User.findOne({ _id: req.params.id, role: "manager" });
    if ("manager" !== manager.role) return res.status(404).json({ message: "Manager Not Found" });
    // Create User instance
    const newUser = new User({
        _id: req.params.id,
        username: req.body.username,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber
    });

    //Update User
    const user = await User.updateOne({ _id: req.params.id }, { $set: newUser });
    if (user !== undefined) {
        res.status(202).json({
            message: "Manager updated"
        })
    } else {
        res.status(404).json({
            message: "Error Update"
        })
    }
};

// Delete User Methode

exports.delete = async (req, res, next) => {
    const result = await User.findOneAndDelete({ _id: req.params.id });
    if (!result) res.status(404).json({
        message: "ERROR TO DELETE"
    });
    else res.status(200).json({
        message: "User Deleted"
    })
};

// upload manager profile image
exports.uploadManagerImage = async (req, res, next) => {

    try {
        const imageUrl = `${req.protocol}://${req.get('host')}/images/${req.file.filename}`;
        if (req.body.id) {
            let manager = await User.findOne({ _id: req.body.id });
            
            if (manager.role == "manager") {
                if(manager.imageUrl) {
                    let oldImage =  manager.imageUrl.split('/images/');
                    oldImage = oldImage[1];
                    let oldPath = path.join(__dirname,"..", "public/images", oldImage); 
                    if (fs.existsSync(oldPath)) {
                        fs.unlink(oldPath,err => {
                            if(err) return res.status(400).json({message:err.message});
                        });
                    }
                }
                const result = await User.updateOne({ _id: manager._id},{ $set: {imageUrl:imageUrl} });
                if(result)  return res.status(200).json({ message: 'Image Manager Updated' });
                else return res.status(400).json({ message: 'Error Occured'});
            }
            else return res.status(404).json({ message: 'Manager Not Found' });
        }
       
    } catch (e) {
        console.log(e);
        return res.status(400).json({ message: e })
    }
}

