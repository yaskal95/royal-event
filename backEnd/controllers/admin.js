const User = require('../models/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


exports.signUp = async (req, res, next) => {

    try {
        if (req.body.username === undefined || req.body.password === undefined) {
            return res.status(400).json({ message: "username or password undifined" });
        }
        const userExist = await User.findOne({ username: req.body.username });
        if (userExist) return res.status(401).json({ message: 'Username alredy exist' });

        if (req.body.password.length > 3) {

            const hash = await bcrypt.hash(req.body.password, 10);
            const admin = new User({
                username: req.body.username,
                password: hash,
                role: "admin"
            });

            const result = await admin.save();

            if (result) {
                return res.status(200).send(true);
            }
        }
    } catch (error) {
        return res.status(error.status).send(false);
    }


}