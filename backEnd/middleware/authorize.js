

module.exports = auhtorize;

function auhtorize(roles = []) {
    return [(req, res, next) => {
    
        if (roles.length && !roles.includes(req.user.role)) {
            // user's role is not authorized
            return res.status(401).json({ message: 'Unauthorized' });
        }

        next();
    }]



}