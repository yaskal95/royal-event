jwt = require('jsonwebtoken');
const User = require('../models/User');

module.exports = async (req,res,next) => {
    
    try{
        const token = req.headers.authorization.split(' ')[1];
        const decodeToken = jwt.verify(token , 'eventSecret');
        const userId = decodeToken.userId;

        if( req.body.userId && req.body.userId !== userId) {
            throw 'User Id not Available';
        } else {
            let user = null;
            if(userId) user = await User.findOne({_id: userId});
            if(user) req.user = user;
            next();
        }
    }catch(e) {
        console.log(e);
        return res.status(401).json({
            message:"Token Expired"
        })
    }

}