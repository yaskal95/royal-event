const XLSX = require('xlsx');

module.exports = (data, sheetName) => {
    const ws = XLSX.utils.json_to_sheet(data);
    const wb = XLSX.utils.book_new();
    if (data.length) {
        ws["!cols"] = Object.keys(data[0]).map(k => ({
            wpx: 200
        }))
    }
    XLSX.utils.book_append_sheet(wb, ws, sheetName);
    const buffer = XLSX.write(wb, {
        type: 'buffer',
        bookType: "xlsx"
    });
    return buffer;
}