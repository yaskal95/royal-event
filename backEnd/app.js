var createError = require('http-errors');
var express = require('express');
var logger = require('morgan');
const cors = require('cors');
const mongoose  = require('mongoose');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var reservationRouter = require('./routes/reservations');
var packRouter = require('./routes/packs');
var cityRouter = require('./routes/cities');
var adminRouter = require('./routes/admins');
const path = require('path');

var app = express();
app.use(cors());

mongoose.connect('mongodb://127.0.0.1:27017/royalEvent' , {useNewUrlParser:true,useUnifiedTopology:true},(err) => {
  if(err) {
    console.log(err);
    return
  }else{
    console.log('Connect to Db Sucess')
  }
});

app.use(express.urlencoded({extended:true}));
app.use(express.json());

//configure express app to serve static files
app.use(express.static(path.join(__dirname, 'public')))

app.use(logger('dev'));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/packs', packRouter);
app.use('/reservations', reservationRouter);
app.use('/cities', cityRouter);
app.use('/admins',adminRouter);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  
  res.setHeader('Access-Control-Allow-Origin', ['*']);
  res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
  
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.json({
    code:err.code,
    meessage:err.message
  });
});

module.exports = app;
