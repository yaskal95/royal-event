var express = require('express');
var router = express.Router();
const userController = require('../controllers/user');
const auth = require('../middleware/auth');
const multer = require('../middleware/multer-config');
const auhtorize = require('../middleware/authorize');

// SignUp Route
router.post('/signup', userController.signUp)
//  Sign in Route
router.post('/signin', userController.signIn);
// Route to update user
router.patch('/update/:id', auth, auhtorize(['admin']), userController.update);
// Route to delete User by Id
router.delete('/delete/:id',auth, auhtorize(['admin']), userController.delete);

// Route to delete User by Id
router.get('/managers/all',auth, auhtorize(['admin']), userController.allManagers);

// Route to get Manager by Id
router.get('/managers/:id', auth,auhtorize(['admin']) ,userController.getManagerById);

// Route to update manager
router.patch('/managers/update/:id', auth,auhtorize(['admin']), userController.updateManager);

// Route to upload profile image manager
router.post('/managers/upload/profile-image',auth ,auhtorize(['admin']) ,multer, userController.uploadManagerImage);

module.exports = router;
