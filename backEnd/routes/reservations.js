var express = require('express');
var router = express.Router();
const reservationController = require('../controllers/reservation');
const auth  = require('../middleware/auth');
const auhtorize = require('../middleware/authorize');

// Route to add new reservation
router.post('/add',auth,auhtorize(['admin','manager']) , reservationController.add);

// Get All reservations
router.get('/all', auth,auhtorize(['admin','manager']) , reservationController.getAll);

// Route to get reservation by id
router.get('/find-one/:id',auth,auhtorize(['admin','manager']) ,reservationController.getReservation);

// Route to det product by id
router.get('/stat/',auth,auhtorize(['admin','manager']) ,reservationController.getStat);

// Update reservation
router.put('/:id',auth,auhtorize(['admin','manager']) ,reservationController.edit);

// Route to delete reservation
router.delete('/:id',auth,auhtorize(['admin','manager'])  ,reservationController.delete);

router.get('/exp', reservationController.exportReservations);

router.get('/exportToCsv', reservationController.exportToCsv);

router.get('/csvToJson', reservationController.csvToJson);

module.exports = router;


