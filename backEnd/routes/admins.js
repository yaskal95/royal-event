var express = require('express');
var router = express.Router();
const adminController = require('../controllers/admin');
const auth = require('../middleware/auth');
const multer = require('../middleware/multer-config');

// SignUp Route
router.post('/sign-up', adminController.signUp)

module.exports = router;
