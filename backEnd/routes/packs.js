var express = require('express');
var router = express.Router();
const packController = require('../controllers/pack');

// SignUp Route
router.post('/add', packController.add)

module.exports = router;
