var express = require('express');
var router = express.Router();
const cityController = require('../controllers/city');

// SignUp Route
router.post('/add', cityController.add);

router.get('/' , cityController.all);

module.exports = router;