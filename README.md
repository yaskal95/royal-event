# RoyalEvent

Mini project is using  to manage an events reservations and users

## Installation

Run npm install to install Back and Front End modules .

##  Usage 

The mini project contains multi part ( Admin and Managers).

Create new admin "localhost:4200/admin-sign-up".

## Techniques Used
- RxJs : ( Observables , Operators , Subject).
- JWT ( Login using Jwt and interceptor ).
- Pagination : ( Pagination Server side using skip and limit , and Client side using NgxPaginate) | Demo : reservations part.
- Filter server side  | Demo : Reservations part
- File Upload Using Multer.
- Export CSV file | Demo : Reservations Part.
 
