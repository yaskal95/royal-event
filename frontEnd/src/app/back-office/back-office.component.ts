import { Component } from '@angular/core';
import { AuthService } from './shared/services/auth.service';

@Component({
  selector: 'app-back-office',
  templateUrl: './back-office.component.html',
  styleUrls: [
    './back-office.component.css'
  ],
})
export class BackOfficeComponent {
  title = 'backOffice';
  isLoggedIn = true;
  constructor(private auth: AuthService) {
    auth.getIsLoggedIn().subscribe(
      (result) => { this.isLoggedIn = result; },
      (error) => { console.log(error) }
    );
  }

  

}