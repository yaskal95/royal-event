import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BackOfficeComponent } from './back-office.component';
import { CityComponent } from './body/city/city.component';
import { AddComponent } from './body/reservation/add/add.component';
import { EditComponent } from './body/reservation/edit/edit.component';
import { ReservationComponent } from './body/reservation/reservation.component';
import { StatisticsComponent } from './body/statistics/statistics.component';
import { AddComponent as AddCityComponent } from './body/city/add/add.component';
import { EditComponent as EditCityComponent } from './body/city/edit/edit.component';
import { SignInComponent } from './body/auth/sign-in/sign-in.component';
import { ManagersComponent } from './body/managers/managers.component';
import { AddManagerComponent } from './body/managers/add/addManager.component';
import { EditManagerComponent } from './body/managers/edit/editManager.component';
import { AuthGuardService as AuthGuard } from './shared/services/auth-guard.service';
import { AdminRegistrationComponent } from './body/auth/sign-up/admin-registration/admin-registration.component';
import { AdminGuardService as AdminGuard } from './shared/services/admin-guard.service';

const routes: Routes = [
  { path : '', component: BackOfficeComponent , children: [
    { path : '', component: StatisticsComponent , canActivate:[ AuthGuard ] },
    { path : 'reservations',  component: ReservationComponent , canActivate:[ AuthGuard ]},
    { path : 'reservations/add', component: AddComponent , canActivate:[ AuthGuard ] },
    { path : 'reservations/edit/:id', component: EditComponent , canActivate:[ AuthGuard ] },
    { path : 'cities', component: CityComponent , canActivate:[ AuthGuard ] },
    { path : 'cities/add', component: AddCityComponent , canActivate:[ AuthGuard ] },
    { path : 'cities/edit/:id', component: EditCityComponent , canActivate:[ AuthGuard ] },
    { path : 'managers', component: ManagersComponent , canActivate:[ AuthGuard , AdminGuard ] },
    { path : 'managers/add', component: AddManagerComponent , canActivate:[ AuthGuard , AdminGuard] },
    { path : 'managers/edit/:id', component: EditManagerComponent , canActivate:[ AuthGuard , AdminGuard ] },
  ]},
  { path :'login' , component: SignInComponent },
  { path :'admin-sign-up' , component: AdminRegistrationComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BackOfficeRoutingModule { }
