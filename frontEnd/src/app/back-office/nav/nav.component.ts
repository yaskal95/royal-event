import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  isAdmin :boolean = false;
  constructor(private authService : AuthService) { }

  ngOnInit(): void {
    this.isAdmin = this.authService.isAdmin();
  }

}
