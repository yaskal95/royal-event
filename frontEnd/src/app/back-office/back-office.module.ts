import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackOfficeRoutingModule } from './back-office-routing.module';
import { HeaderComponent } from './header/header.component';
import { NavComponent } from './nav/nav.component';
import { FooterComponent } from './footer/footer.component';
import { BackOfficeComponent } from './back-office.component';
import { StatisticsComponent } from './body/statistics/statistics.component';
import { ReservationComponent } from './body/reservation/reservation.component';
import { AddComponent } from './body/reservation/add/add.component';
import { EditComponent } from './body/reservation/edit/edit.component';
import { AddComponent as AddCityComponent } from './body/city/add/add.component';
import { EditComponent as EditCityComponent } from './body/city/edit/edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AlertDeleteComponent } from './shared/component/alert-delete/alert-delete.component';
import { ModalDetailsComponent } from './shared/component/modal-details/modal-details.component';
import { CityComponent } from './body/city/city.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { SignInComponent } from './body/auth/sign-in/sign-in.component';
import { ManagersComponent } from './body/managers/managers.component';
import { EditPasswordComponent } from './body/managers/edit-password/edit-password.component';
import { AddManagerComponent } from './body/managers/add/addManager.component';
import { EditManagerComponent } from './body/managers/edit/editManager.component';
import { AuthInterceptor } from './shared/interceptors/auth.interceptor';
import { ChangeColorDirective } from './shared/directive/change-color.directive';
import { NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { FileUploadModule } from 'ng2-file-upload';
import { AdminRegistrationComponent } from './body/auth/sign-up/admin-registration/admin-registration.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    BackOfficeComponent,
    HeaderComponent,
    NavComponent,
    FooterComponent,
    StatisticsComponent,
    ReservationComponent,
    AddComponent,
    EditComponent,
    AlertDeleteComponent,
    ModalDetailsComponent,
    CityComponent,
    AddCityComponent,
    EditCityComponent,
    SignInComponent,
    ManagersComponent,
    EditPasswordComponent,
    AddManagerComponent,
    EditManagerComponent,
    ChangeColorDirective,
    AdminRegistrationComponent,
  ],
  imports: [
    CommonModule,
    NgxPaginationModule,
    BackOfficeRoutingModule,
    FormsModule,
    HttpClientModule,
    NgSelectModule,
    NgbPopoverModule,
    ReactiveFormsModule,
    FileUploadModule,

  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ]
})
export class BackOfficeModule { }
