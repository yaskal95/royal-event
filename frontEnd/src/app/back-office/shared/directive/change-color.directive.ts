import { Directive, ElementRef, HostBinding, HostListener, Input, OnChanges, OnInit,SimpleChange } from '@angular/core';

@Directive({
  selector: '[changeColor]'
})
export class ChangeColorDirective implements OnInit {

  @Input() changeColor: String = "new";
  constructor(private elRef: ElementRef) { }

  @HostBinding('backgroundColor') backgroundColor?:String;

  ngOnInit(): void {
    if(this.changeColor === "new") this.elRef.nativeElement.style.backgroundColor = "#12CEF5";
    else if(this.changeColor === "inProcess") this.elRef.nativeElement.style.backgroundColor = "#F39C12";
    else if(this.changeColor === "canceled") this.elRef.nativeElement.style.backgroundColor = "#EE3316";
    else if(this.changeColor === "done") this.elRef.nativeElement.style.backgroundColor = "#24E758";
    
    this.elRef.nativeElement.style.color = "white";
  }

  @HostListener('mouseout') clickButton() {
    if(this.changeColor === "new") this.elRef.nativeElement.style.backgroundColor = "#12CEF5";
    else if(this.changeColor === "inProcess") this.elRef.nativeElement.style.backgroundColor = "#F39C12";
    else if(this.changeColor === "canceled") this.elRef.nativeElement.style.backgroundColor = "#EE3316";
    else if(this.changeColor === "done") this.elRef.nativeElement.style.backgroundColor = "#24E758";
  }

  

  
}
