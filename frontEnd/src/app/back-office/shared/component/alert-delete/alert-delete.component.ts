import { Component, OnInit , Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-alert-delete',
  templateUrl: './alert-delete.component.html',
  styleUrls: ['./alert-delete.component.css']
})
export class AlertDeleteComponent implements OnInit {
  @Input() message: String = "";
  @Input() title: String = "";
  constructor(public activalModal : NgbActiveModal) {}

  ngOnInit(): void {
  }

}
