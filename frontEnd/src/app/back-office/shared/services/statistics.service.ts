import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StatisticsService {

  readonly BASE_URL_Reservations = "http://localhost:3000/reservations/stat";
  readonly BASE_URL_Managers = "http://localhost:3000/users/managers/stat";

  constructor( private http :HttpClient ) { }


  getReservationsStatistics():Observable<any> {
    return this.http.get<any>(`${this.BASE_URL_Reservations}`);
  }
}
