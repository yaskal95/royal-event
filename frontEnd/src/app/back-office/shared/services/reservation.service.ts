import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

export interface IReservation {
  customerName: String,
  cin: String,
  phoneNumber: String,
  description: String,
  priceAdvance: Number,
  totalPrice: Number,
  createdAt: Date,
  reservationAt:Date,
  city:String,
  address:String,
  status: String
}

@Injectable({
  providedIn: 'root'
})
export class ReservationService {
  
  readonly BASE_URL = "http://localhost:3000/reservations";
  
  constructor( private http : HttpClient) {
    
  }

  add(reservation:IReservation) : Observable<{done: boolean}> {
    return this.http.post<{done:boolean}>(`${this.BASE_URL}/add`,reservation);
  }

  // Get All Reservations
  getReservations(expands?:String[],page?:number,status?:string) : Observable<any[]> {
    return this.http.get<any[]>(`${this.BASE_URL}/all?expands=${expands}&status=${status}&page=${page}`);
  }

  // Find Reservation by Id 
  getReservationById(id : String) : Observable<any> {
    return this.http.get<any[]>(`${this.BASE_URL}/find-one/`+id);
  }

  // Update Reservation Method 
  update(reservation:any) : Observable<{done:boolean}> {
    return this.http.put<{done: boolean}>(`${this.BASE_URL}/`+reservation._id,reservation);
  }

  // Delete Reservation Method
  delete(reservation:any) : Observable<{done:boolean}> {
    return this.http.delete<{done: boolean}>(`${this.BASE_URL}/`+reservation._id);
  }

  // Export Csv file
  export():void {
    const url = `/reservations/exportToCsv`;
    console.log(window.location.origin.includes('localhost') ? `:3000${url}` : url);
    const baseUrl = window.location.origin.split(':4200')[0];

    document.location.href =`${baseUrl}:3000${url}`;

  }
  
}
