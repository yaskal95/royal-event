import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  readonly BASE_URL = "http://localhost:3000/admins"
  constructor(private http:HttpClient) { }

  signUp(admin:any): Observable<{done: boolean}> {
    console.log(admin);
    return this.http.post<{done: boolean}>(`${this.BASE_URL}/sign-up`,admin);
  }
}
