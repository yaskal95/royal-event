import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

export interface IManager {
  username: String,
  email: String,
  phoneNumber: String,
  role?: String,
  password?: String
}

@Injectable({
  providedIn: 'root'
})
export class ManagersService {

  readonly BASE_URL = "http://localhost:3000/users";
  // signin
  constructor( private http : HttpClient) { }

  add( manager :IManager) : Observable<{done: boolean}>{
    manager.role = "manager";
    return this.http.post<{done: boolean}>(`${this.BASE_URL}/signup`,manager);
  }

  // Get All Managers
  getManagers(expands?:String[],page?:number) : Observable<any[]> {
    return this.http.get<any[]>(`${this.BASE_URL}/managers/all?expands=${expands}&page=${page}`);
  }

  // get Manager by Id
  getManager(id: string):Observable<any> {
    return this.http.get<any>(`${this.BASE_URL}/managers/${id}`);
  }

  // update Manager information
  update(id:string,manager:IManager) {
    return this.http.patch<any>(`${this.BASE_URL}/managers/update/${id}`,manager);
  }

  export(): Observable<any[]> {
    return this.http.get<any[]>(`${this.BASE_URL}/managers/export`);
  }
  
}
