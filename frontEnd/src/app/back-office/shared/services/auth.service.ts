import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs'; 
import { Router } from '@angular/router';
import {Location} from '@angular/common';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private isLoggedIn = new BehaviorSubject<boolean>(false);
  readonly BASE_URL = "http://localhost:3000/users";

  constructor(private http: HttpClient , private router:Router, private location: Location) { 
    
    if(localStorage.getItem("token")) {
      this.isLoggedIn.next(true);
    }
  }

  signIn(user:any) :Observable<any>{
    return this.http.post<any>(`${this.BASE_URL}/signin`,user);
  }

  setSession = async (authResult:any) => {
    await localStorage.setItem('token', authResult.token);
    await localStorage.setItem('user', JSON.stringify(authResult.user));
    if (localStorage.getItem('token')) this.isLoggedIn.next(true);
  };

  logout() {
    this.isLoggedIn.next(false);
    localStorage.clear();
    this.router.navigate(['login']);
  }

  getIsLoggedIn() {
    return this.isLoggedIn.asObservable();
  }

  getCurrentUser(){
    var currentUser:any = JSON.parse(''+localStorage.getItem("user")+'');
    delete currentUser.password;
    
    if(currentUser) return currentUser;
    else return null;
  }

  isAdmin() :boolean {
    if(this.getCurrentUser().role === "admin")  return true ;
    return false;
  }

}
