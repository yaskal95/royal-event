import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ImageUploaderService {

  private managerUrl: string ="http://localhost:3000/users/managers/upload/profile-image";
  
  constructor(private http:HttpClient) { }

  uploadManagerImage(id:String,file:File):Observable<any> {
    let formData :FormData = new FormData();
    formData.append("image",file,file.name);
    formData.append("id",id.toString());

    return this.http.post<any>(`${this.managerUrl}`,formData);
  }

  getManagerUrl() {
    return this.managerUrl;
  }
  
}
