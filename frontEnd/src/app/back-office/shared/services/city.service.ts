import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  readonly BASE_URL = "http://localhost:3000/cities";
  
  constructor( private http : HttpClient) { 
    
  }

  add(city:any) : Observable<{done: boolean}> {
    return this.http.post<{done:boolean}>(`${this.BASE_URL}/add`,city);
  }

  // Get All cities
  getCities() : Observable<any[]> {
    return this.http.get<any[]>(`${this.BASE_URL}/`);
  }

  // Find city by Id 
  getCity(id : String) : Observable<any> {
    return this.http.get<any[]>(`${this.BASE_URL}/`+id);
  }

  // Update city Method 
  update(city:any) : Observable<{done:boolean}> {
    return this.http.put<{done: boolean}>(`${this.BASE_URL}/`+city._id,city);
  }

  // Delete city Method
  delete(city:any) : Observable<{done:boolean}> {
    return this.http.delete<{done: boolean}>(`${this.BASE_URL}/`+city._id);
  }
}
