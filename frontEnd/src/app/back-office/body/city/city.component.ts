import { Component, OnInit } from '@angular/core';
import { CityService } from '../../shared/services/city.service';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css']
})
export class CityComponent implements OnInit {

  cities : any[] = [];
  p: number = 1;
  
  constructor( private cityService : CityService) { }

  ngOnInit(): void {
    this.getCities();
  }

  getCities () {
    this.cityService.getCities().subscribe((response: any) => {
      this.cities = response.cities;
    })
  }

}
