import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CityService } from 'src/app/back-office/shared/services/city.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  isLoading = false;
  alert = {
    type: 'success',
    message: "",
    active: false,
  };
  city :any = { name:""};

  constructor( private cityService:CityService) { }

  ngOnInit(): void {
  }

  onSubmit(form:NgForm) {
    this.isLoading = true;
    this.alert.active = false;
    this.cityService.add(this.city).subscribe((result) => {
      this.isLoading = false;
      form.resetForm();
      this.alert = {
        type: 'success',
        message: "City had been saved successfully",
        active: true,
      };
    } , (err) => {
      this.alert = {
        type: 'danger',
        message: "Error occured while saving a City",
        active: true,
      };
    })
  }
}
