import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FileItem, FileUploader } from 'ng2-file-upload';
import { AlertDeleteComponent } from '../../shared/component/alert-delete/alert-delete.component';
import { ImageUploaderService } from '../../shared/services/image-uploader.service';
import { ManagersService } from '../../shared/services/managers-service.service';

@Component({
  selector: 'app-managers',
  templateUrl: './managers.component.html',
  styleUrls: ['./managers.component.css']
})
export class ManagersComponent implements OnInit {

  public managers : any [] = [];
  file!:File;
  usersImages :any [] = [];
  
  constructor(
    private modalService: NgbModal,
    private managersService :ManagersService,
    private imageUploader: ImageUploaderService,) { }

  ngOnInit(): void {
    this.getManagers();
  }

  getManagers() {
    this.managersService.getManagers().subscribe( 
      (response :any) => {
        this.managers = response.users;
      }
    );
  }

  // Method to delete Manager
  deleteConfirm ( reservation :any) {
    const modalRef = this.modalService.open(AlertDeleteComponent,{size:"md"});
    modalRef.componentInstance.message = "Are you sure you want to delete this Manager";
    modalRef.componentInstance.title = "Delete confirmation";
    modalRef.result.then(result => {
      if(result == "ok") {
        const index = this.managers.indexOf(reservation);
        this.managers.splice(index, 1);
        // this.managersService.deleteReservation(reservation).subscribe((respone:any) => {
        //   console.log(respone);
        // } , (err) => {
        //   console.log(err);
        // })
      };
    })
  }
  
  change(id:string,event:any){
    let user = {
      id:id,
      file:event.target.files[0]
    }
    if(this.usersImages.length>0){
      let index = this.usersImages.findIndex((e)=> e.id===id);
      let user = {
        id:id,
        file:event.target.files[0]
      };
      if(index !== -1) {
        this.usersImages[index].file = event.target.files[0];
      }else{
        this.usersImages.push(user);
      }
    }else{
      this.usersImages.push(user);
    };
  }
  // Upload manager image
  uploadProfileImage(id:String,) {
    
    if(this.usersImages.length > 0) {
      let index = this.usersImages.findIndex((e)=> e.id===id);
      this.imageUploader.uploadManagerImage(id,this.usersImages[index].file).subscribe(
        (response) => {console.log(response)} ,
        (err) => {console.log(err)}
      );
    }
  }

  export() {
    this.managersService.export().subscribe(
      (response) => {console.log(response)},
      (err) => {console.log(err)}
    );

  }

}
