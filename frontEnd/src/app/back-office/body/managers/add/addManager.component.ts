import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { IManager, ManagersService} from 'src/app/back-office/shared/services/managers-service.service';

@Component({
  selector: 'app-add-manager',
  templateUrl: './addManager.component.html',
  styleUrls: ['./addManager.component.css']
})
export class AddManagerComponent implements OnInit {

  isLoading = false;
  alert = {
    type: 'success',
    message: "",
    active: false,
  }

  manager : IManager = {
    username:"",
    email:"", 
    phoneNumber : "",
    password:"",
  };

  constructor( private managersService :ManagersService) { }

  ngOnInit(): void {
  }
  
  onSubmit(form :NgForm) {
    this.isLoading = true;
    this.managersService.add(this.manager).subscribe((respone) => {
      this.isLoading = false;
      form.resetForm();
      this.alert = {
        type: 'success',
        message: "Manager had been saved successfully",
        active: true,
      };
    }, (err) => {
      console.log(err);
      this.isLoading = false;
    });
  }

}
