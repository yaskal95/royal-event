import { Component, OnInit } from '@angular/core';
import {FormGroup , FormControl, Validators, EmailValidator} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IManager, ManagersService } from 'src/app/back-office/shared/services/managers-service.service';

@Component({
  selector: 'app-edit',
  templateUrl: './editManager.component.html',
  styleUrls: ['./editManager.component.css']
})
export class EditManagerComponent implements OnInit {
  isLoading = false;
  alert = {
    type: 'success',
    message: "",
    active: false,
  }; 
  manager:IManager = {
    username:"",
    email:"",
    phoneNumber:"",
  };

  id!:string;

  editManagerForm = new FormGroup({
    username: new FormControl('',[Validators.required,Validators.minLength(3)]),
    phoneNumber: new FormControl(''),
    email: new FormControl('',[Validators.required,Validators.email]),
  });

  constructor(private activatedRoute :ActivatedRoute , private managerService :ManagersService) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.managerService.getManager(this.id).subscribe(
      (result) => {
        this.manager = result;
        this.initForm(this.manager);
      },
      (err) => {console.log(err);}
    );
  }

  initForm(manager:IManager) {
    this.editManagerForm.get('username')?.setValue(manager.username);
    this.editManagerForm.get('email')?.setValue(manager.email);
    this.editManagerForm.get('phoneNumber')?.setValue(manager.phoneNumber);
  }

  onSubmit() {
    this.isLoading = true;
    let formValue = this.editManagerForm.value;
    this.manager.username = formValue.username;
    this.manager.email = formValue.email;
    this.manager.phoneNumber = formValue.phoneNumber;

    this.managerService.update(this.id,this.manager).subscribe(
      (result) => {
        this.alert.message =  "Manager updated";
        this.alert.active = true;
        this.isLoading = false;
      } ,
      (err) => {console.log("update result "+err);}
      );
  }

}