import { Component, OnInit } from '@angular/core';
import { StatisticsService } from '../../shared/services/statistics.service';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {
  
  reservationsStat :any;
  managersCount:number = 0;
  constructor( private statService: StatisticsService) { }

  ngOnInit(): void {
    this.getReservationsStatistics();
  }

  getReservationsStatistics() {
    this.statService.getReservationsStatistics().subscribe(
      (res) => {
        this.reservationsStat = res;
      }, (err) => {
        console.log(err);
      }
    )
  };

}
