import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CityService } from 'src/app/back-office/shared/services/city.service';
import { IReservation, ReservationService } from 'src/app/back-office/shared/services/reservation.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css'],
  providers: [DatePipe]
})
export class AddComponent implements OnInit {

  isLoading = false;
  selectedCity !: any;
  alert = {
    type: 'success',
    message: "",
    active: false,
  }

  reservation: IReservation = {
    customerName: "",
    cin: "",
    description: "",
    phoneNumber: "",
    priceAdvance: 0.00,
    totalPrice: 0.00,
    createdAt: new Date(),
    reservationAt: new Date(),
    city: "",
    address: "",
    status: "new"
  };

  cities : any [] = [];

  constructor(private reservationService: ReservationService , private cityService: CityService) { }

  ngOnInit(): void { 
    this.getCities();
  }

  onSubmit(form: NgForm) {
    this.isLoading = true;
    this.reservationService.add(this.reservation).subscribe((respone) => {
      this.isLoading = false;
      form.resetForm();
      this.alert = {
        type: 'success',
        message: "Reservation had been saved successfully",
        active: true,
      };
    }, (err) => {
      console.log(err);
      this.isLoading = false;
    });
  }

  // checkDate() {
  //   const dateSendingToServer = new DatePipe('en-US').transform(this.reservation.reservationAt, 'dd/MM/yyyy')
  //   console.log(dateSendingToServer);
  // }

  getCities() {
    this.cityService.getCities().subscribe((response: any) => {
      this.cities = response.cities;
    })
  }
}
