import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IReservation, ReservationService } from 'src/app/back-office/shared/services/reservation.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  isLoading = false;
  id !:String;
  res = {};
  reservation:any;

  alert = {
    type: 'success',
    message: "",
    active: false,
  }

  constructor( private activatedRoute: ActivatedRoute , private reservationService: ReservationService) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.reservationService.getReservationById(this.id).subscribe((res:any) => {
      this.reservation = res;
      console.log(this.reservation.reservationAt);
    });
    
  }

  onSubmit(form:NgForm) {

    this.isLoading = true;
    this.reservationService.update(this.reservation).subscribe((respone) => {
      this.isLoading = false;
      this.alert = {
        type: 'success',
        message: "Reservation had been updated successfully",
        active: true,
      };

    }, (err) => {
      
      this.isLoading = false;
    });

  }

}
