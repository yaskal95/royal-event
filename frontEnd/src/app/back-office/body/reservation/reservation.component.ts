import { Component, OnInit } from '@angular/core';
import { NgbModal , NgbPopover} from '@ng-bootstrap/ng-bootstrap';
import { AlertDeleteComponent } from '../../shared/component/alert-delete/alert-delete.component';
import { ModalDetailsComponent } from '../../shared/component/modal-details/modal-details.component';
import { AuthService } from '../../shared/services/auth.service';
import { ReservationService } from '../../shared/services/reservation.service';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {
  
  public reservations : any [] = [];
  pages = [1];
  currentPage :number = 1;
  
  constructor( 
    private reservationService: ReservationService,
    private modalService :NgbModal,
    private authService: AuthService
    ) { }

  ngOnInit(): void {
    this.getReservations();
  }

  // MEthod to get all reservations
  getReservations(page:number =1 , status?:string) {
    let expands = ["city"];
    this.currentPage = page;
    this.reservationService.getReservations(expands,page,status).subscribe((response: any) => {
      this.reservations = response.reservations;
      this.pages = Array(response.totalPages ).map((x,i)=>i);
    },
    (err) => {console.log(err)},
    )
  }

  // Method to delete Reservation
  deleteConfirm ( reservation :any) {
    const modalRef = this.modalService.open(AlertDeleteComponent,{size:"md"});
    modalRef.componentInstance.message = "Are you sure you want to delete this reservation";
    modalRef.componentInstance.title = "Delete confirmation";
    modalRef.result.then(result => {
      if(result == "ok") {
        const index = this.reservations.indexOf(reservation);
        this.reservations.splice(index, 1);
        this.reservationService.delete(reservation).subscribe((respone:any) => {
          console.log(respone);
        } , (err) => {
          console.log(err);
        })
      };
    })
  }

  // Method to show reservation details in Modal
  details(reservation:any) {
    const modalRef = this.modalService.open(ModalDetailsComponent,{size:"lg"});
    modalRef.componentInstance.reservation = reservation;
    console.log(reservation);
    modalRef.result.then(result => {
      
    });
  }

  // Pagination
  next() {
    this.currentPage< this.pages.length ? this.currentPage ++ : this.currentPage;
    this.getReservations(this.currentPage);
  }
  // Pagination
  pervious() {
    this.currentPage>1 ? this.currentPage -- : this.currentPage;
    this.getReservations(this.currentPage);
  }

  // To open popup
  openPop(pop : NgbPopover) {
      pop.open();
  }

  // Change status to done
  done( reservation :any) {
    reservation.status = "done";
    this.updateReservation(reservation);    
  }

  // Change status to cancel
  cancel(reservation :any) {
    reservation.status = "canceled";
    this.updateReservation(reservation);    
  }
  
  // Change status to inProcess
  process(reservation :any) {
    reservation.status = "inProcess";
    this.updateReservation(reservation);
  }

  updateReservation(reservation:any) {
    this.reservationService.update(reservation).subscribe(
      (res) => {
        console.log(res);
      } , 
      (err) => { console.log(err);}
    );
  }

  filter(value:any) {
    this.currentPage = 1;
    if(value == "done" || value == "canceled" || value == "new" || value == "inProcess" || value == "all") {
      this.getReservations(this.currentPage,value);
    }
  }
  // Export reservations to csv file
  export(){
    this.reservationService.export();
  }
}
