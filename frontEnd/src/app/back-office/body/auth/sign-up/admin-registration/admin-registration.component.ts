import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AdminService } from 'src/app/back-office/shared/services/admin.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-admin-registration',
  templateUrl: './admin-registration.component.html',
  styleUrls: ['./admin-registration.component.css']
})
export class AdminRegistrationComponent implements OnInit {

  isLoading = false;
  adminForm :FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required,Validators.minLength(3)]),
    password: new FormControl('', [Validators.required])
  });
  admin = {
    username:undefined,
    password:undefined
  }

  constructor( private adminService :AdminService , private toastr:ToastrService ) { }

  ngOnInit(): void {
  }

  // Submit registration
  onSubmit(){
    this.isLoading = true;
    const formValue = this.adminForm.value;
    this.admin.username = formValue.username;
    this.admin.password = formValue.password;

    this.adminService.signUp(this.admin).subscribe(
      (result) => {
        this.toastr.success("Registration success");
        this.isLoading = false;
        this.adminForm.reset();
      },
      (err) => {
        this.isLoading = false;
      }
    )

  }

}
