import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/back-office/shared/services/auth.service';
import { IManager } from 'src/app/back-office/shared/services/managers-service.service';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  isLoading = false;
  isUndefined = false;
  isWrongPassword = false;
  user = {
    username: '',
    password: '',
  };
  constructor( private auth: AuthService , private router: Router,private location: Location) { }

  ngOnInit(): void {
  }

  signIn() {
    this.isLoading = true;
    this.auth.signIn(this.user).subscribe(
      async (result) => {
      this.isLoading = false;
      await this.auth.setSession(result);
      this.router.navigate(['admin']);
      
    } ,(err) => {
      console.log(err.error);
      err.error.message === "Wrong Username" ? this.isUndefined = true :this.isUndefined = false ;
      err.error.message === "Wrong Password" ? this.isWrongPassword = true : this.isWrongPassword = false ;
      this.isLoading = false;
    });
  }

}
